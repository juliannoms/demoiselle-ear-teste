# EAR/EJB com Framework Demoiselle #

O objetivo é demonstrar o erro que ocorre ao fazer o deploy de aplicação EAR com EJB que depende do demoiselle-core.

Ambiente
 - SO: Linux;
 - JRE: Oracle JDK 1.6_45;
 - JBOSS EAP 6.2;
 - Framework Demoiselle.

Passos para simular o erro
 - No projeto ejb -> mvn install;
 - No projeto ear -> mvn package;
 - Inicializar o JBOSS com standalone-full.xml;
 - Colocar o arquivo gerado na pasta target do projeto ear (ear-teste.ear) na pasta /standalone/deployments/ do servidor.