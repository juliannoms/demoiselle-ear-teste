package com.juliannoms.ejb;


import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.util.ResourceBundle;

@Stateless
public class HelloWorld {
	
	@Inject
	private Logger logger;
	
	@Inject
	private ResourceBundle bundle;
	
	public void say() {
		logger.info(bundle.getString("say_hello"));
	}
}
